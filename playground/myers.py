import numpy as np

ALPHABET = "ACGT"

def myers(t, p, k, w, c):
	n, m = len(t), len(p)
	B = dict()
	for x in ALPHABET:
		B[x] = np.uint32(0)
	for j in range(c):
		B[p[j]] |= 1 << (w - c + j)

	VP = np.uint32(2 ** w - 1)
	VN = np.uint32(0)
	score = c - 1

	for pos in range(n):
		for x in ALPHABET:
			B[x] >>= 1
		if pos + c < m:
			B[p[pos + c]] |= 1 << (w - 1)

		X = B[t[pos]] | VN
		#print("B_c_ref", B[t[pos]], "VN", np.uint32(VN), "X", np.uint32(X))
		D0 = np.uint32(((VP + (X & VP)) ^ VP) | X)
		HN = VP & D0
		HP = VN | ~ (VP | D0)
		X = D0 >> 1
		VN = X & HP
		VP = HN | ~ (X | HP)
		#print("VP", np.uint32(VP), "VN", np.uint32(VN), "HP", HP, "HN", HN)

		if pos <= m - c:
			score += 1 - ((D0 >> (w - 1)) & 1)
			#print("D0", D0)
		else:
			s = (w - 2) - (pos - (m - c + 1))
			#print(s, "HP", HP, "HN", HN)
			score += (HP >> s) & 1
			score -= (HN >> s) & 1
			#print(pos < m - c, t[pos], pos, score, m, s)
		if pos >= m - c and score <= k:
			print("    occurence:", pos, score)


t = 'TTTTGTTATGTTGTCTAGGTTTTTTTACTTGTCGCTTACTCATAAGACACACACGACCTC'
p = 'TACTCATAAGACACACACGACCTC'
myers(t, p, 50, 32, 10)





def myers_identity(t, p, k, w, c):
	n, m = len(t), len(p)
	B = dict()
	for x in ALPHABET:
		B[x] = 0
	for j in range(c):
		B[p[j]] |= 1 << (w - c + j)
	
	VP = 2 ** w - 1
	VN = 0
	score = -c

	for pos in range(n):
		for x in ALPHABET:
			B[x] >>= 1
		if pos + c < m:
			B[p[pos + c]] |= 1 << (w - 1)

		X = B[t[pos]] | VN
		D0 = ((VP + (X & VP)) ^ VP) | X
		HN = VP & D0
		HP = VN | ~ (VP | D0)
		X = D0 >> 1
		VN = X & HP
		VP = HN | ~ (X | HP)

		if pos <= m - c:
			score += ((D0 >> (w -1)) & 1)
		else:
			s = (w - 2) - (pos - (m - c + 1))
			score += ((HN >> s) & 1) - ((HP >> s) & 1)
		print(t[pos], pos, score, m)
		if pos >= m - 1 - c and score <= k:
			print("occurence:", pos, score)


def myers_edit(t, p, k, w, c, local=False):
	n, m = len(t), len(p)
	B = dict()
	for x in ALPHABET:
		B[x] = 0
	for j in range(c):
		B[p[j]] |= 1 << (w - c + j)
	
	# for edit distance, avoid zeros in first row
	VP = (2 ** (c + 1) - 1) << (w - c - 1)
	print(bin(VP))
	VN = 0
	editdist = c
	identity = 0
	score = -c

	for pos in range(n):
		for x in ALPHABET:
			B[x] >>= 1
		if pos + c < m:
			B[p[pos + c]] |= 1 << (w - 1)

		X = B[t[pos]] | VN
		D0 = ((VP + (X & VP)) ^ VP) | X
		HN = VP & D0
		HP = VN | ~ (VP | D0)
		X = D0 >> 1
		VN = X & HP
		VP = HN | ~ (X | HP)
		#print("VP", bin(VP))
		#print("VN", bin(VN))

		if pos < m - c - 1:
			match = ((D0 >> (w -1)) & 1)
			print("match", match)
			editdist += 1 - match
			identity += match
			score += 2 * match - 1
			if local:
				score = max(0, score)
		else:
			s = (w - 2) - (pos - (m - c + 1))
			plus = ((HP >> s) & 1)
			minus = ((HN >> s) & 1)
			print("horizontal", pos, m - c)
			print("match", minus)
			print("edit", plus)
			editdist += plus - minus
			identity += minus - plus
			score += minus - plus
		print(t[pos], pos, editdist, identity, identity - editdist, score)
		if pos >= m - 1 - c and editdist <= k:
			print("occurence:", pos, score)




def myers_longest_prefix(t, p, k, w, c):
	n, m = len(t), len(p)
	B = dict()
	for x in ALPHABET:
		B[x] = 0
	for j in range(c):
		B[p[j]] |= 1 << (w - c + j)
	
	# for edit distance, avoid zeros in first row
	VP = (2 ** (c + 1) - 1) << (w - c - 1)
	VN = 0
	score = c
	identity = 0
	best_score = 0
	best_pos = 0

	for pos in range(n):
		for x in ALPHABET:
			B[x] >>= 1
		if pos + c < m:
			B[p[pos + c]] |= 1 << (w - 1)

		X = B[t[pos]] | VN
		D0 = ((VP + (X & VP)) ^ VP) | X
		HN = VP & D0
		HP = VN | ~ (VP | D0)
		X = D0 >> 1
		VN = X & HP
		VP = HN | ~ (X | HP)

		if pos <= m - c:
			match = ((D0 >> (w -1)) & 1)
			score += 1 - match
			#score += match * 2 - 1
		else:
			s = (w - 2) - (pos - (m - c + 1))
			plus = ((HP >> s) & 1)
			minus = ((HN >> s) & 1)

			score += plus - minus
			identity += - ((HP >> s) & 1) + ((HN >> s) & 1)
		print(t[pos], pos, score, identity)
		if score < best_score:
			best_score = score
			best_pos = pos
			print("best score", pos, relscore, score)
	print("edit distance", score)



t = "ACCCGTTTA"
t2 = "AAAAAAAAAAAAAATTTT"
p = "ACACGTTTA"
p2 = "GTTTA"
p3 = "ACCCG"
p4 = "AGTTTA"
p5 = "TTTT"

#t = "TTTTTTTTTTTTTTTTTAAAAA"
#p = "TTTTTTTTAAAAATT"
#myers(t, p, 3, 32, 12)

#t = "AAATTTTT" + "T" * 100 + "A" * 100
#p = "A" * 20 + "T" * 44
#myers(t, p, 50, 32, 20)



#print("longest prefix")
#myers_longest_prefix(t, p, 1, 32, 4)
