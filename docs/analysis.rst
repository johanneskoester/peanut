========================================================
Analysis pipeline for PEANUT performance and sensitivity
========================================================

The following `Snakemake <http://bitbucket.org/johanneskoester/snakemake>`_ pipeline conducts the
complete performance and sensitivity analysis of PEANUT and its competitors as presented in our publication.
The corresponding Snakefile can be downloaded :download:`here <../analysis-pipeline/Snakefile>`.

.. literalinclude:: ../analysis-pipeline/Snakefile
   :language: python
   :tab-width: 4
