
.. highlight:: bash

.. default-role:: command

.. include:: ../README.rst

License
=======

PEANUT is available under the :doc:`MIT license </license>`.

Analysis Pipeline
=================

The pipeline used for the analysis done in the paper can be obtained :doc:`here </analysis>`.

Author
======

.. include:: ../AUTHORS.rst
