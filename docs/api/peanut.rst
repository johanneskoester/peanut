====================
PEANUT API Reference
====================

.. contents::
   :local:
   :depth: 2


Module contents
===============

.. automodule:: peanut
    :members:
    :exclude-members: Hits
    :show-inheritance:


Submodules
==========

peanut.alignment module
-----------------------

.. automodule:: peanut.alignment
    :members:
    :show-inheritance:

peanut.bitencoding module
-------------------------

.. automodule:: peanut.bitencoding
    :members:
    :show-inheritance:

peanut.filtration module
------------------------

.. automodule:: peanut.filtration
    :members:
    :show-inheritance:

peanut.input module
-------------------

.. automodule:: peanut.input
    :members:
    :show-inheritance:

peanut.output module
--------------------

.. automodule:: peanut.output
    :members:
    :show-inheritance:

peanut.postprocessing module
----------------------------

.. automodule:: peanut.postprocessing
    :members:
    :show-inheritance:

peanut.query module
-------------------

.. automodule:: peanut.query
    :members:
    :exclude-members: Queries
    :show-inheritance:

peanut.reference module
-----------------------

.. automodule:: peanut.reference
    :members:
    :exclude-members: References
    :show-inheritance:

peanut.stats module
-------------------

.. automodule:: peanut.stats
    :members:
    :show-inheritance:

peanut.utils module
-------------------

.. automodule:: peanut.utils
    :members:
    :show-inheritance:

peanut.validation module
------------------------

.. automodule:: peanut.validation
    :members:
    :show-inheritance:

peanut.version module
---------------------

.. automodule:: peanut.version
    :members:
    :show-inheritance:
