valgrind --tool=memcheck --leak-check=full --num-callers=30 --suppressions=valgrind-python.supp python3 setup.py nosetests 2> valgrind.out
